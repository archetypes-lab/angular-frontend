# Angular Frontend App 

<h2 id="req-previos-container">Requerimientos previos - Instalación Remote Container</h2> 

La instalación de <a href="https://code.visualstudio.com/docs/remote/containers">remote container</a> permite desarrollar dentro de un contenedor docker utilizando visual studio code de forma remota a este. Automaticamente se monta el ambiente de desarrollo, con los requerimientos y addons necesarios, sin embargo, el rendimiento no es tan bueno como seria en una instalación normal.

Se requiere tener instalados los siguientes componentes para el desarrollo en windows 10 o en linux.
* <a href="https://hub.docker.com/editions/community/docker-ce-desktop-windows"> Docker Community Edition o Docker for Windows</a>.

<p style="color: red;font-weight: bold">Si se desarrolla en windows se recomienda tener instalado el soporte WSL 2, para mejorar el rendimiento, ya que de manera contraria se puede ver negativamente afectado. En ese caso se recomienda una instalación normal. Aun asi el rendimiento para proyectos con muchos archivos como node_modules perjudica notoriamente el rendimiento en Windows</p>

<img src="https://i.ibb.co/28Q3kMT/image-7.png" />

Para ver como instalar WSL 2 click <a href="https://docs.docker.com/docker-for-windows/wsl/">aqui</a>

***
<h2 id="req-previos-normal">Requerimientos previos - Instalación Normal</h2>

La instalación normal, solamente ocupa la maquina HOST con visual studio code,
para montar el proyecto. Requiere la instalación y configuración de todos los componentes en la maquina host.

Se requiere tener instalados los siguientes componentes para el desarrollo en windows 10 o en linux.
* <a href="https://git-scm.com/downloads" >GitBash</a>. Ultima versión. 
* <a href="https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html">Java JDK 11 Amazon Correto</a>. Para uso sonarlint.
* <a href="https://github.com/coreybutler/nvm-windows">Node Version Manager NVM</a>. Ultima versión.
* <a href="https://code.visualstudio.com">Visual Studio Code</a>. Ultima versión.

### Verificación Git

Se recomienda una versión igual o superior de Git for Windows como se ve en la imagen, ya que
se pueden presentar problemas con la integración git con visual studio code.

<img src="https://i.ibb.co/hsr1hbV/image-3.png" />


### Configuración versión de Node

1. Utilizando NVM es necesario instalar Node versión 12.18.2
2. Abrir una consola gitbash y ejecutar: 
    ```
    nvm install 12.18.2
    ```
3. Luego ejecutar: 
    ```
    nvm use 12.18.2
    ```
4. Verificar que la instalación fue exitosa ejecutando: 
    ```
    node -v
    ``` 
    lo que debería mostrar "v12.8.2"

### Verificación Java 

1. Descargar el instalador automático (exe en windows) de Java 11 Amazon Corretto.
2. Verificar la versión de java en consola ejecutando: "java -version", debería entregar algo asi:

<img src="https://i.ibb.co/mvZqXK2/image-1.png"/>

3. La variable JAVA_HOME debe estar apuntando al JDK de Amazon Corretto:

<img src="https://i.ibb.co/805dWHV/image-2.png" />

### Configuración Visual Studio Code

#### Configurar addons

Las addons de visual studio nos permiten transformar el editor de texto en un IDE necesario para trabajar en el proyecto.

Para instalar un addons, hacer paso 1 y 2 como se ve en la imagen y luego presionar sobre el botón install del addons.

<img src="https://i.ibb.co/JdwM4nb/image-4.png" />

Los addons a instalar son los siguientes:

* sonarsource.sonarlint-vscode
* ms-vscode.vscode-typescript-next
* dbaeumer.vscode-eslint
* angular.ng-template
* johnpapa.angular2
* sibiraj-s.vscode-scss-formatter

Reiniciar visual studio code, luego de instalar todos estos addons.

***

## MONTAR PROYECTO

1) Clonar la ruta del proyecto con Git.
2) Abrir la carpeta con visual studio code.

Si se quiere montar el proyecto de forma normal:
* <a href="#req-previos-normal">Instalar requerimientos previos de instalación normal</a>
* <a href="#configurar-addons">Configurar addons de visual studio code</a>

Si se quiere montar el proyecto con remote container, realizar paso 1 y 2 de la siguiente imagen. 
(requiere tener instalado el addons de remote container 'ms-vscode-remote.remote-containers' )

<img src="https://i.ibb.co/4p3nWS1/image-8.png" />


3) Copiar el archivo /src/app/assets/config/env.json.template
a /src/app/assets/config/env.json. Fijarse de copiar el recurso y no moverlo.

4) Configurar las variables de ambiente en env.json. 
    * 'apiUrl' : Apunta al Api Backend.

<p style="color: red; font-weight: bold">
Nota: Si se desarrolla en contenedor se puede utilizar 'host.docker.internal' para hacer referencia al host desde el contenedor.
</p>

<img src="https://i.ibb.co/p4ybVQ3/image-9.png" />

5) La primera vez o si hay un cambio de librerías ejecutar <a href="#comandos">(ver sección comandos)</a>:
    
    ```
    npm install
    ```
6) Para iniciar el proyecto ejecutar en consola <a href="#comandos">(ver sección comandos)</a>:
    ```
    npm start
    ```
7) Para que el sistema funcione correctamente, el <a href="https://gitlab.com/archetypes-lab/springboot-backend">backend</a> debe estar funcionando en la url especificada en env.json

## COMANDOS
1) Abrir terminal "bash" en visual studio code.

<img src="https://i.ibb.co/0cSddWF/image-6.png" />

1) Para construir proyecto:
    ```
    npm install
    ```
2) Para lanzar proyecto en modo desarrollo:
    ```
    npm start
    ```
3) Para construir carpeta dist:
    ```
    npm run build-local
    ```

## GENERACIÓN API BACKEND - SWAGGER

Para realizar cambios en el API hacia el backend, 
el proceso se realiza automáticamente realizando los siguientes
pasos.

1) La primera vez es necesario instalar de forma global el utilitario generador
    ```
    npm install @openapitools/openapi-generator-cli -g
    ```

2) En la raíz del proyecto ejecutar 
    ```
    openapi-generator generate -i http://localhost:8080/v2/api-docs -g typescript-angular -o src/app/core/api
    ```

    donde "http://localhost:8080/v2/api-docs" es la especificación json swagger del api backend.

## GENERACIÓN DE ARTEFACTOS CON ANGULAR-CLI

1) Para generar un modulo con routing:
    * En consola posicionarse en el directorio base del proyecto, donde se encuentra angular.json y package.json
    * Ejecutar en la consola:
    
        ```
        npm run ng generate module mi-modulo -- --routing
        ```

    * El modulo quedar construido en el directio {base}/src/app/mi-modulo. En su interior se generar el objeto que describe al modulo llamado "mi-modulo.module.ts" y el modulo de routing "mi-modulo-routing.module.ts"


2) Para generar componentes dentro de un modulo:
    *   Con un  consola posicionarse en el directorio base del proyecto, donde se encuentra angular.json y package.json
    *  Ejecutar:
    
        ```
        npm run ng generate component mi-modulo/mi-componente
        ```
    
    * Sera construida una carpeta del componente dentro de la carpeta del modulo, quedando la siguiente estructura {base}/src/app/mi-modulo/mi-componente. 
    * Dentro de la carpeta del componente se construiran el html, el ts, el css y el test.
    * Se recomienda agregar un comentario al interior del archivo del css para evitar problemas con inspección de código. Ejemplo: 
        ```
        /* csss style mi-componente */ 
        ```

## CONSEJOS AL DESARROLLAR
 * Utilizar las <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf">shortcuts</a> al desarrollar:
    * <strong>control + p</strong>: búsqueda de archivos
    * <strong>alt + shift + o</strong>: ordenar imports automaticamente
    * <strong>alt + shift +f</strong>: auto formatear código
    * <strong>ctrl + shift + k</strong>: Eliminar linea
    * <strong>alt + shift + flecha abajo</strong>: Duplicar linea
    * <strong>F2</strong>: refactorizar