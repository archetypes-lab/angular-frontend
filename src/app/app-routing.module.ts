
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IdentidadGuard } from './core/guards/identidad.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: './home/home.module#HomeModule'
    },
    {
        path: 'showcase',
        loadChildren: './showcase/showcase.module#ShowcaseModule'
    },
    {
        path: 'usuarios', canActivate: [IdentidadGuard],
        loadChildren: './usuarios/usuarios.module#UsuariosModule'
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
