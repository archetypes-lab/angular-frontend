
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermitAllGuard } from '../core/guards/permit-all.guard';
import { TemplateComponent } from '../shared-template/template/template.component';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
    {
        path: '', component: TemplateComponent, canActivate: [PermitAllGuard],
        children: [
            {path: '', component: IndexComponent, outlet: 'content'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShowcaseRoutingModule {}