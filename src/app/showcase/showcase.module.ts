import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
/* primeng */
import { ButtonModule } from 'primeng/button';
import { SharedTemplateModule } from '../shared-template/shared-template.module';
import { IndexComponent } from './index/index.component';
import { ShowcaseRoutingModule } from './showcase-routing.module';


@NgModule({
    declarations: [IndexComponent],
    imports: [
        CommonModule,
        ShowcaseRoutingModule,
        SharedTemplateModule,
        /*primeng*/
        ButtonModule
    ]
})
export class ShowcaseModule { }
