import { Component, Injector, OnInit } from '@angular/core';
import { ShowcaseRestService } from 'app/core/api';
import { BlockuiService } from 'app/core/services/blockui.service';
import { BreadcrumbService } from 'app/core/services/breadcrumb.service';
import { BreadcrumbDto } from 'app/core/dto/breadcrumb.dto';
import { HttpService } from 'app/core/services/http.service';
import { saveAs } from 'file-saver';
import { SimpleMessageService } from 'app/core/services/simple-message.service';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

    private readonly showcaseApi: ShowcaseRestService;
    private readonly breadcrumbService: BreadcrumbService;
    private readonly blockui: BlockuiService;
    private readonly httpService: HttpService;
    private readonly simpleMessageService: SimpleMessageService;

    constructor(private injector: Injector) {
        this.showcaseApi = this.injector.get(ShowcaseRestService);
        this.blockui = this.injector.get(BlockuiService);
        this.breadcrumbService = this.injector.get(BreadcrumbService);
        this.httpService = this.injector.get(HttpService);
        this.simpleMessageService = this.injector.get(SimpleMessageService);
    }

    ngOnInit() {
        this.breadcrumbService.configDesdeHome([<BreadcrumbDto>{
            label: 'showcase'
        }])
    }

    public async lanzarErrorNegocioAction() {
        this.blockui.block();
        try {
            await this.showcaseApi.lanzarErrorNegocioUsingGET().toPromise();
            console.log('fue lanzada excepción de negocio');
        } catch (error) {
            console.log('llego un error de negocio');
        }
        this.blockui.tick();
    }

    public async lanzarErrorInternoAction() {
        this.blockui.block();
        try {
            await this.showcaseApi.lanzarErrorInternoUsingGET().toPromise();
            console.log('fue lanzado un error interno');
        } catch (error) {
            console.log('llego un error interno');
        }
        this.blockui.tick();
    }

    public lanzarErrorEntradaAction() {
        // queda pendiente
    }

    public async accederAccionAdministradorAction() {
        this.blockui.block();
        try {
            await this.showcaseApi.accionAdministradorUsingGET().toPromise();
            console.log('si llegue aqui es porque tengo permiso de admin');
        } catch (error) {
            console.log('si llegue aqui es porque no tengo permiso de admin');
        }
        this.blockui.tick();
    }

    public async procesarEnBackendAction() {
        this.blockui.block();
        console.log('mandando a dormir');
        await this.showcaseApi.sleepUsingGET().toPromise();
        console.log('ya desperte!');
        this.blockui.tick();
    }

    public async crearArchivo() {
        this.blockui.block();
        await this.showcaseApi.generarYSubirArchivoUsingPOST().toPromise();
        this.blockui.tick();
        this.simpleMessageService.defaultSuccessMsg('se creo el archivo en el backend');
    }

    public async eliminarArchivo() {
        this.blockui.block();
        await this.showcaseApi.eliminarArchivoUsingPOST().toPromise();
        this.blockui.tick();
        this.simpleMessageService.defaultSuccessMsg('Se elimino el archivo en el backend');
    }

    public async descargarArchivo() {
        this.blockui.block();
        const data = await this.httpService.post('/showcase/descargar-archivo', {}, 'blob').toPromise();
        this.blockui.tick();
        saveAs(data, 'hola-mundo.pdf');
    }

    public async enviarCorreo() {
        this.blockui.block();
        await this.showcaseApi.enviarEmailUsingPOST().toPromise();
        this.blockui.tick();
        this.simpleMessageService.defaultSuccessMsg('Se envio e-mail');
    }

}
