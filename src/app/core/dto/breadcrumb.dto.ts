export interface BreadcrumbDto {
    label:string,
    routerLink?:string,
    icon?: string
}
