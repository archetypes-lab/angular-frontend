export interface IdentidadDto {
    idPersona: number;
    username: string;
    run: number;
    email: string;
    nombres: string;
    apPaterno: string;
    apMaterno: string;
    perfil: PerfilDto;
}

export interface PerfilDto {
    idPerfil: number;
    nombre: string;
}