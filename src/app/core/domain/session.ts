import { IdentidadDto } from "../dto/identidad.dto";

export interface Session {
    identidad: IdentidadDto;
    csrf: string
}
