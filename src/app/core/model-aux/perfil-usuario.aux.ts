import { SelectItem } from 'primeng/api';
import { PerfilUsuario } from '../api';

export class PerfilUsuarioAux {

    public static readonly ID_TIPO_PERFIL_INSTITUCION_PLAN: number = 5
    public static readonly ID_TIPO_PERFIL_INSTITUCION_INICIATIVA: number = 6

    public static perfilToSelectItem(perfil: PerfilUsuario): SelectItem {
        return <SelectItem>{ label: perfil.nombre, value: perfil };
    }
}
