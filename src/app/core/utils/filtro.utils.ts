
export class FiltroUtils {

  static omitNonNumbers(event: any) {
    const charCode = event.charCode;
    return charCode > 47 && charCode < 58;
  }

}
