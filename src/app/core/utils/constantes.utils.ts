import { SelectItem } from 'primeng/api';

export class ConstantesUtils {

    /* routes */
    static readonly ROUTE_INICIO: string = '';
    static readonly ROUTE_SHOWCASE: string = 'showcase';
    static readonly ROUTE_GESTIONAR_USUARIOS: string = 'usuarios';
    static readonly ROUTE_EDITAR_USUARIO: string = 'usuarios/editar';
    static readonly ROUTE_LOGIN: string = 'login';
    static readonly ROUTE_LOGOUT: string = 'logout';

    /* id perfiles */
    static readonly ID_PERFIL_ADMINISTRADOR: number = 1;

    /* parametros flash scope */
    static readonly FS_KEY_ID_USUARIO: string = 'ID_USUARIO_KEY';

    static readonly MAX_VALUE_INT: number = 2147483647;
    static readonly MAX_VALUE: number = 999999999;

    /** [1000000, 99999999] Rango Valido Para el RUT */
    static readonly RANGO_MIN_RUT_VALIDO = 1000000;
    static readonly RANGO_MAX_RUT_VALIDO = 99999999;

    private constructor() {
    }

    static createSeleccionarSelectItem() {
        return <SelectItem>{ label: 'Seleccionar', value: null };
    }

    static createTodasFiltroSelectItem() {
        return <SelectItem>{ label: 'Todas', value: null };
    }

    static createTodosSelectItem() {
        return <SelectItem>{ label: 'Todos', value: null };
    }

    static createHabilitadosSelectItems(): SelectItem[] {
        return [
            ConstantesUtils.createTodosSelectItem(),
            { label: 'Habilitado', value: true },
            { label: 'Deshabilitado', value: false }
        ];
    }

}
