import * as moment from 'moment';

export class DateUtils {

    public static parcheFecha12Horas(date: Date): Date {
        if (!date) {
            return null;
        }
        const salidaDate: Date = new Date(date);
        salidaDate.setHours(12);
        return salidaDate;
    }

    public static stringToDate(dateString: string): Date {
        return moment(dateString).toDate();
    }

    public static createUTC(dateInput: Date): Date {
        return new Date(Date.UTC(
            dateInput.getFullYear(),
            dateInput.getMonth(),
            dateInput.getDate(),
            dateInput.getHours(),
            dateInput.getMinutes(),
            dateInput.getSeconds(),
            dateInput.getMilliseconds()));
    }

    public static getMonthName(mes: number): string {
        const date = new Date(2000, mes - 1, 1);
        return date.toLocaleString('es', { month: 'long' });
    }

    public static getPreviousMonthDate(currentDate: Date): Date {
        const currentMoment = moment(currentDate);
        return currentMoment.add(-1, 'months').toDate();
    }

    public static getFechaDeCierreConTiempo(fechaDeCierre: Date): Date {
        fechaDeCierre.setHours(23);
        fechaDeCierre.setMinutes(59);
        fechaDeCierre.setSeconds(59);
        fechaDeCierre.setMilliseconds(999);
        return fechaDeCierre;
    }

    public static getFechaDeStringConFormato(fecha: string, formato: string): Date {
        const m = moment(fecha, formato);
        return m.toDate();
    }
}
