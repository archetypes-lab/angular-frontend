/**
 * Spring Boot Backend
 * Especificación para Spring Boot Backend
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Comuna } from './comuna';


export interface Persona { 
    apellidoMaterno?: string;
    apellidoPaterno?: string;
    comuna?: Comuna;
    email?: string;
    fechaNacimiento?: string;
    idPersona?: number;
    nombres?: string;
    run?: number;
    rutaArchivoImagenPerfil?: string;
    telefono?: string;
}

