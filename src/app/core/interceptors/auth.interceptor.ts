import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
  })
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const csrfToken = this.authService.getCsrfToken();
        const httpHeaders = csrfToken ? req.headers.set('X-CSRF-TOKEN', csrfToken) : req.headers;
        const authReq = req.clone({headers: httpHeaders, withCredentials: true});
        return next.handle(authReq);
    }
}


