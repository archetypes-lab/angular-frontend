import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let updatedRequest = req;
    if (req.headers.get('accept') === 'application/octet-stream') {
      updatedRequest = req.clone({ responseType: 'blob' });
    }
    return next.handle(updatedRequest);
  }
}
