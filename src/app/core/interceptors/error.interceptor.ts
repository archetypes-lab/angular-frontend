import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DialogMessageService } from '../services/dialog-message.service';

@Injectable({
    providedIn: 'root'
  })
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private dialogMessageService: DialogMessageService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .catch(errorResponse => {
                switch (errorResponse.status) {
                    case 403:
                    case 401:
                            this.router.navigate(['/']);
                            this.dialogMessageService.showAccesoDenegado();
                        break;
                    case 500:
                        const errorBackend: ErrorBackend = <ErrorBackend>errorResponse.error;
                        if (errorBackend.errorType === 'LogicaNegocioException') {
                            this.dialogMessageService.showLogicaNegocioError(errorBackend.message, errorBackend.values);
                            //Se muestra el Dialogo de logica Negocio, pero no se muestra por consola la respuesta del servidor.
                            return Observable.of();
                        }
                        else if (errorBackend.errorType === 'OptimisticLocking') {
                            this.dialogMessageService.showDatosDesactualizados();
                        } else {
                            this.dialogMessageService.showErrorInterno(errorBackend.values);
                        }
                        return Observable.throw(errorBackend);
                    default:
                        this.dialogMessageService.showErrorInterno();
                        break;
                }
                let errMsg: string;
                if (errorResponse instanceof HttpErrorResponse) {
                    const err = errorResponse.message || JSON.stringify(errorResponse.error);
                    errMsg = `${errorResponse.status} - ${errorResponse.statusText || ''} Details: ${err}`;
                } else {
                    errMsg = errorResponse.message ? errorResponse.message : errorResponse.toString();
                }
                return Observable.throw(errMsg);
            });
    }
}

class ErrorBackend {
    errorType: string;
    message: string;
    values: any;
}
