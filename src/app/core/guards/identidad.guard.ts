import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { IdentidadService } from '../services/identidad.service';

@Injectable({
    providedIn: 'root'
  })
export class IdentidadGuard implements CanActivate {

    constructor(private identidadService: IdentidadService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(!this.identidadService.tieneIdentidad()) {
            return this.accesoDenegado();
        }
        return true;
    }

    private accesoDenegado() : boolean {
        this.router.navigate(['/']);
        window.alert("No tienes permiso para acceder a esta página");
        return false;
    }

}

