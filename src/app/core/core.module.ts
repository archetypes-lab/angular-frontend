import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, enableProdMode, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgIdleModule } from '@ng-idle/core';
/* Translate */
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CurrencyMaskModule, CURRENCY_MASK_CONFIG } from "ng2-currency-mask";
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
/* primeng */
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ToastModule } from 'primeng/toast';
/* Apis */
import { ApiModule as ApiGatewayApiModule, BASE_PATH as ApiGatewayApiBasePath } from './api';
import { BlockuiComponent } from './blockui/blockui.component';
import { CustomCurrencyMaskConfig } from './config/custom-currency-mask-config';
import { DialogMessageComponent } from './dialog-message/dialog-message.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { FileInterceptor } from './interceptors/file.interceptor';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { AppConfigService } from './services/app-config.service';
import { AuthService } from './services/auth.service';
import { ShellComponent } from './shell/shell.component';


/* AoT requires an exported function for factories */
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function apiBasePathFactory(appConfigService: AppConfigService) {
  return appConfigService.apiUrl;
}

export function appInit(appConfigService: AppConfigService) {
  return async () => {
    await appConfigService.load(); 
  }
}

@NgModule({
  declarations: [
    BlockuiComponent,
    ShellComponent,
    DialogMessageComponent,
    ScrollTopComponent,
  ],
  imports: [
    /* primeng */
    DialogModule,
    ButtonModule,
    ScrollPanelModule,
    ProgressSpinnerModule,
    ToastModule,
    CurrencyMaskModule,
    /* apis */
    ApiGatewayApiModule,
    /* otros */
    CommonModule,
    HttpClientModule,
    NgIdleModule.forRoot(),
    NgxWebstorageModule.forRoot(),
    RouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    HttpClient,
    MessageService,
    ConfirmationService,
    // HTTP INTERCEPTORS
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FileInterceptor,
      multi: true
    },
    // Api Base Paths
    {
      provide: ApiGatewayApiBasePath,
      useFactory: apiBasePathFactory,
      deps: [AppConfigService]
    },
    // Others
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: CURRENCY_MASK_CONFIG,
      useValue: CustomCurrencyMaskConfig
    },
    // APP INITIALIZER
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [AppConfigService]
    }
  ],
  exports: [
    ShellComponent]
}
)
export class CoreModule { }
