import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/api';

@Injectable({
    providedIn: 'root'
})
export class SimpleMessageService {

    constructor(private messageService: MessageService, private translate: TranslateService) {

    }

    public defaultSuccessMsg(msg?: string, title?: string): void {
        this.defaultMsg('success', title ? title : 'dialogs.info.title', msg ? msg : 'dialogs.global-messages.exito.message' );
    }

    public defaultErrorMsg(msg: string, title?: string): void {
        this.defaultMsg('error', title ? title : 'dialogs.error.title', msg);
    }

    private defaultMsg(severityParam: string, titleParam: string, msgParam: string): void {
        this.messageService.add({
            severity: severityParam,
            summary: this.translate.instant(titleParam),
            detail: this.translate.instant(msgParam)
        });
    }



}
