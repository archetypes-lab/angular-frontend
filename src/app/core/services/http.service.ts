import { HttpClient } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";
import { AppConfigService } from "./app-config.service";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class HttpService {


    private readonly appConfigService: AppConfigService;
    private readonly httpClient: HttpClient;
    private readonly authService: AuthService;


    constructor(private injector: Injector) {
        this.httpClient = injector.get(HttpClient);
        this.authService = injector.get(AuthService);
        this.appConfigService = injector.get(AppConfigService);
    }

    public post(path: string, body?: any, responseTypeParam?: string): Observable<any> {
        const options = this.getOptions(responseTypeParam);
        return this.httpClient.post(`${this.appConfigService.apiUrl}${path}`, body, options);
    }

    // private

    private getOptions(responseTypeParam?: string): any {
        const csrf = this.authService.getCsrfToken();
        return {
            headers: [
                { 'X-CSRF-TOKEN': [csrf] }
            ],
            responseType: responseTypeParam ?? 'application/json'
        };
    }

}