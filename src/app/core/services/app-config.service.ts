import { Injectable } from "@angular/core";
import { default as axios } from 'axios';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {

    private _production: boolean;
    private _envName: string;
    private _apiUrl: string;

    public async load(): Promise<any> {
        return await axios.get('/assets/config/env.json')
            .then(response => {
               this._production = response.data.production;
               this._envName = response.data.envName;
               this._apiUrl = response.data.apiUrl;
                return response;
            });
    }

    get production(): boolean {
        return this._production;
    }

    get envName(): string {
        return this._envName;
    }

    get apiUrl(): string {
        return this._apiUrl;
    }

}