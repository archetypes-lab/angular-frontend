import { Injectable, Injector } from "@angular/core";
import { IdentidadDto } from "../dto/identidad.dto";
import { ConstantesUtils } from "../utils/constantes.utils";
import { AuthService } from "./auth.service";


@Injectable({
    providedIn: 'root'
})
export class IdentidadService {

    /* injecciones */
    private readonly authService: AuthService;

    public constructor(private injector: Injector) {
        this.authService = injector.get(AuthService);
    }

    public get identidad(): IdentidadDto {
        if (this.authService.isLogged()) {
            return this.authService.getSessionData();
        } else {
            return null as IdentidadDto;
        }
    }

    public esAdministador(): boolean {
        const identidad: IdentidadDto = this.identidad;
        return (identidad !== null 
            && identidad.perfil !== null 
            && identidad.perfil.idPerfil === ConstantesUtils.ID_PERFIL_ADMINISTRADOR);
    }

    public tieneIdentidad(): boolean {
        return this.identidad !== null;
    }

}
