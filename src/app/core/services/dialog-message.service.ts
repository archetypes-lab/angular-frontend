import { Injectable, Injector } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DialogMessageService {

    private translateService: TranslateService;
    private notifyCloseDialogSource = new Subject();
    private openDialogSource = new Subject();

    notifyCloseDialog$ = this.notifyCloseDialogSource.asObservable();
    openDialog$ = this.openDialogSource.asObservable();

    constructor(injector: Injector) {
        this.translateService = injector.get(TranslateService);
    }

    notifyCloseDialog() {
        this.notifyCloseDialogSource.next();
    }

    openDialog(parameters: DialogMessageData) {
        this.openDialogSource.next(parameters);
    }

    public showAccesoDenegado() {
        const title = this.translateService.instant('dialogs.global-messages.error.acceso-denegado.title');
        const message = this.translateService.instant('dialogs.global-messages.error.acceso-denegado.message');
        this.showError(title, message);
    }

    public showDatosDesactualizados() {
        const title = this.translateService.instant('dialogs.global-messages.error.datos-desactualizados.title');
        const message = this.translateService.instant('dialogs.global-messages.error.datos-desactualizados.message');
        this.showError(title, message, "labels.refrescar", () => location.reload());
    }

    public showErrorInterno(values?: any, title?: string, message?: string) {
        if (!title) {
            title = this.translateService.instant('dialogs.global-messages.error.interno.title');
        }

        if (!message) {
            message = this.translateService.instant('dialogs.global-messages.error.interno.message');
        }

        if (values && values.stacktrace) {
            message =
                `
            <p><strong>${message}</strong></p>
            <p><em>Stacktrace: </em></p>
            <p>${values.stacktrace}</p>
            `
        }

        this.showError(title, message);
    }

    public showLogicaNegocioError(message: string, values: any, title?: string) {
        if (!title) {
            title = this.translateService.instant('dialogs.global-messages.error.negocio.title');
        }
        if (values && values.errores) {
            const errorArray = <string[]>values.errores;
            let ulErrors = '<ul>';
            errorArray.forEach(e => ulErrors += `<li>${e}</li>`);
            ulErrors += '</ul>';
            this.showError(title, ulErrors);
        } else {
            this.showError(title, message);
        }
    }

    public showError(title: string, message: string, closeButtonLabel?: string, closeFunction?: Function) {
        const data: DialogMessageData = <DialogMessageData>{
            title: title,
            message: message,
            severity: Severity.Error,
            closeButtonLabelCode: closeButtonLabel,
            closeFunction: closeFunction
        };
        this.openDialog(data);
    }

    public showSuccess(title?: string, message?: string, closeFunction?: Function) {
        if (!title) {
            title = this.translateService.instant('dialogs.global-messages.exito.title');
        }
        if (!message) {
            message = this.translateService.instant('dialogs.global-messages.exito.message');
        }
        const parameters: DialogMessageData = <DialogMessageData>{
            title: title,
            message: message,
            severity: Severity.Success,
            closeFunction: closeFunction
        };
        this.openDialog(parameters);
    }
}

export interface DialogMessageData {
    title: string;
    message: string;
    severity: Severity;
    closeButtonLabelCode?: string;
    closeFunction?: Function;
}

export enum Severity {
    Info,
    Error,
    Success
}
