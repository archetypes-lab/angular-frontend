import { formatDate } from '@angular/common';
import { Injectable, Injector } from '@angular/core';
import { TableHeader } from 'app/shared-commons/interfaces/table-header';
import * as _ from 'lodash';
import { TreeNode } from 'primeng/api';
import { BlockuiService } from './blockui.service';

@Injectable({
  providedIn: 'root'
})


export class ExportDataService {

  private readonly blockuiService: BlockuiService;

  constructor(injector: Injector) {
    this.blockuiService = injector.get(BlockuiService);
  }

  /**
   * Exporta datos de una p-table a un documento Excel.
   * @param excelColumns Las columnas que se incluirán en el archivo Excel.
   * @param data Los datos de la tabla.
   */
  public exportTabularDataToExcel(excelColumns: TableHeader[], data: any[]) {
    this.blockuiService.block(1);
    const json = this.generateJsonFromTabularData(excelColumns, data);
    this.exportExcel(json);
    this.blockuiService.tick();
  }

  /**
   * Exporta datos (homogéneos) de un p-treeTable a un documento Excel.
   * @param excelColumns Las columnas que se incluirán en el archivo Excel.
   * @param treeData Los datos homogéneos (todos los nodos son del mismo tipo).
   */
  public exportHomogeneousTreeDataToExcel(excelColumns: TableHeader[], treeData: TreeNode[]) {
    this.blockuiService.block(1);
    let dataList: any[] = [];
    treeData.forEach(tree => {
      dataList = dataList.concat(this.getListFromHomogeneousTree(tree));
    });
    const json = this.generateJsonFromTabularData(excelColumns, dataList);
    this.exportExcel(json);
    this.blockuiService.tick();
  }

  /**
   * Exporta datos (heterogéneos) de un p-treeTable a un documento Excel.
   * @param excelColumns Las columnas que se incluirán en el archivo Excel.
   * @param treeData Los datos heterogéneos (todos los nodos son de tipos diferentes y sus propiedades no se traslapan).
   */
  public exportHeterogeneousTreeDataToExcel(excelColumns: TableHeader[], treeData: TreeNode[]) {
    this.blockuiService.block(1);
    let dataList: any[] = [];
    treeData.forEach(tree => {
      dataList = dataList.concat(this.getListFromHeterogeneousTree(tree, null));
    });
    const json = this.generateJsonFromTabularData(excelColumns, dataList);
    this.exportExcel(json);
    this.blockuiService.tick();
  }

  private generateJsonFromTabularData(excelColumns: TableHeader[], data: any[]): any[] {
    const json: any[] = [];
    if (data && data.length > 0) {
      // CASO TABLA LLENA
      data.forEach(dataElement => {
        const row = {};
        excelColumns.forEach(column => {
          row[column.header] =
            (dataElement[column.field] || dataElement[column.field] === 0) ?
              dataElement[column.field].toString() : '';
        });
        json.push(row);
      });
    } else {
      // CASO TABLA VACIA
      const row = {};
      excelColumns.forEach(column => {
        row[column.header] = '';
      });
      json.push(row);
    }

    return json;
  }
  /**
   * Método que convierte un árbol homogéneo en una lista de objetos
   * @param nodo La raíz de un árbol homogéneo (con nodos del mismo tipo).
   */
  private getListFromHomogeneousTree(nodo: TreeNode): any[] {
    let dataList: any[] = [];
    dataList.push(nodo.data);
    if (nodo.children && nodo.children.length > 0) {
      nodo.children.forEach(child => {
        dataList = dataList.concat(this.getListFromHomogeneousTree(child));
      });
    }
    return dataList;
  }

  /**
   * Método que convierte un árbol heterogéneo en una lista de objetos.
   * @param nodo La raíz de un árbol heterogéneo (todos los nodos son de tipos diferentes y sus propiedades no se traslapan).
   * @param datosAncestros Objeto que contiene los datos de los antepasados del nodo actual.
   */
  private getListFromHeterogeneousTree(nodo: TreeNode, datosAncestros: any): any[] {
    let dataList: any[] = [];
    const datosAcumulados = {};
    if (datosAncestros) {
      _.merge(datosAcumulados, datosAncestros);
    }
    _.merge(datosAcumulados, nodo.data);
    if (nodo.children && nodo.children.length > 0) {
      nodo.children.forEach(child => {
        dataList = dataList.concat(this.getListFromHeterogeneousTree(child, datosAcumulados));
      });
    } else {
      dataList.push(datosAcumulados);
    }
    return dataList;
  }
  private exportExcel(json: any[]) {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(json);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'datos_exportados');
    });
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
      const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      const EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
    });
  }

  public formatDateToExport(date: Date, dateFormat: string = 'dd/MM/yyyy'): string {
    return (!date) ? '' : formatDate(new Date(date), dateFormat, 'en-US');
  }
}
