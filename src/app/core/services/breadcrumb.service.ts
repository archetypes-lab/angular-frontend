import { Injectable } from "@angular/core";
import * as _ from 'lodash';
import { Subject } from "rxjs";
import { BreadcrumbDto } from "../dto/breadcrumb.dto";

@Injectable({
    providedIn: 'root'
})
export class BreadcrumbService {

    private newConfigSource = new Subject<BreadcrumbDto[]>();
    public newConfig$ = this.newConfigSource.asObservable();

    public configDeshabilitado(): void {
        return this.config([]);
    }

    public configDesdeModUsuarios(breadcrumbs?: BreadcrumbDto[]): void {
        const usuariosBreadcrumb = <BreadcrumbDto>{
            label: 'Usuarios',
            routerLink: '/usuarios'
        };
        const breadsFinal = breadcrumbs ? _.concat([usuariosBreadcrumb], breadcrumbs) : [usuariosBreadcrumb];
        return this.configDesdeHome(breadsFinal);
    }

    public configDesdeHome(breadcrumbs?: BreadcrumbDto[]): void {
        const homeBreadcrumb = <BreadcrumbDto>{
            label: 'Inicio',
            routerLink: '/',
            icon: 'fa fa-fw fa-home'
        };
        const breadsFinal = breadcrumbs ? _.concat([homeBreadcrumb], breadcrumbs) : [homeBreadcrumb];
        return this.config(breadsFinal);
    }

    /* privados */

    private config(breadcrumbs?: BreadcrumbDto[]): void {
        if (breadcrumbs) {
            this.newConfigSource.next(breadcrumbs);
        }
    }

}
