import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { CredencialesDto, Identidad, SecurityRestService } from '../api';
import { Session } from '../domain/session';
import { IdentidadDto, PerfilDto } from '../dto/identidad.dto';
import { ConstantesUtils } from '../utils/constantes.utils';


@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private session: Session;

    /* injecciones */

    private readonly router: Router;
    private readonly sessionService: SessionStorageService;
    private readonly securityApi: SecurityRestService;

    public constructor(private injector: Injector) {
        this.router = injector.get(Router);
        this.sessionService = injector.get(SessionStorageService);
        this.securityApi = injector.get(SecurityRestService);
        this.session = <Session>{};
    }

    /* PUBLICOS */

    public async initSecurity(): Promise<void> {
        await this.cargarCsrfToken();
        await this.intentarEstablecerSesion();
    }

    public async login(username: string, password: string): Promise<void> {
        await this.cargarCsrfToken();
        const respuesta = await this.securityApi.loginUsingPOST(<CredencialesDto>{
            username: username,
            password: password
        }).toPromise();
        if (respuesta.exitoLogin) {
            const identidadDto = this.mapToIdentidadDto(respuesta.identidad);
            this.guardarSesion(identidadDto);
        }
        else {
            throw new Error(JSON.stringify(respuesta.errores));
        }
    }

    public isLogged(): boolean {
        return (this.session !== undefined && this.session !== null) && this.session.identidad !== null;
    }

    public getSessionData(): IdentidadDto {
        return (this.session && this.session.identidad) ? this.session.identidad : null;
    }

    public cleanSession(): void {
        this.session = <Session>{};
        this.sessionService.clear();
    }

    public getCsrfToken(): string {
        return (this.session && this.session.csrf) ? this.session.csrf : null;
    }

    public async logout() {
        await this.securityApi.logoutUsingGET().toPromise();
        this.cleanSession();
        await this.cargarCsrfToken();
        this.router.navigate([ConstantesUtils.ROUTE_INICIO]);
    }

    /*
    PRIVADOS
     */

    private mapToIdentidadDto(identidad: Identidad): IdentidadDto {
        return <IdentidadDto>{
            apMaterno: identidad.apMaterno,
            apPaterno: identidad.apPaterno,
            email: identidad.email,
            idPersona: identidad.idPersona,
            nombres: identidad.nombres,
            perfil: <PerfilDto>{
                idPerfil: identidad.idPerfil,
                nombre: identidad.nombrePerfil
            },
            run: identidad.run,
            username: identidad.username
        };
    }

    private guardarSesion(identidadDto: IdentidadDto) {
        this.session.identidad = identidadDto;
        console.log(`se creo session: ${JSON.stringify(this.session.identidad)}`);
    }


    private async cargarCsrfToken(): Promise<void> {
        const crsfToken = await this.securityApi.getCsrfTokenUsingGET().toPromise();
        console.log(`csrf: ${crsfToken.value}`);
        this.session.csrf = crsfToken.value;
    }

    private async intentarEstablecerSesion(): Promise<void> {
        try {
            const identidad = await this.securityApi.getIdentidadUsingPOST().toPromise();
            if (identidad) {
                const identidadDto = this.mapToIdentidadDto(identidad);
                this.guardarSesion(identidadDto);
            }
        } catch (error) {
            // no hace nada
            this.session.identidad = null;
            return Promise.resolve();
        }
    }

}
