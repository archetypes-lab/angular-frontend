export enum Permiso {

    // MODULO PLANES
    /**
     * Permiso: Gestionar plan
     */
    PL_GP = 'PL-GP',

    /**
     * Permiso: Acceso buscador planes
     */
    PL_ABP = 'PL-ABP',

    /**
     * Permiso: Ver plan
     */
    PL_VP = 'PL-VP',

    /**
     * Permiso: Modificar estado del plan
     */
    PL_MEP = 'PL-MEP',

    // MODULO INICIATIVAS

    /**
     * Permiso: Gestionar iniciativa
     */
    IN_GI = 'IN-GI',

    /**
     * Permiso: Acceso buscador iniciativas
     */
    IN_ABI = 'IN-ABI',

    /**
     * Permiso: Ver Iniciativa
     */
    IN_VI = 'IN-VI',

    /**
     * Permiso: Asignar meta principal
     */
    IN_AM = 'IN-AMP',

    /**
     * Permiso: Modificar estado de iniciativa
     */
    IN_MEI = 'IN-MEI',

    // MODULO SEGUIMIENTO

    /**
     * Permiso: Acceso bandeja de seguimiento
     */
    SE_ABS = 'SE-ABS',

    /**
     * Permiso: Ver rendiciones vigentes
     */
    SE_VRV = 'SE-VRV',

    /**
     * Permiso: Ver rendiciones historicas
     */
    SE_VRH = 'SE-VRH',

    /**
     * Permiso: Ingresar rendición vigente
     */
    SE_IRV = 'SE-IRV',

    /**
     * Permiso: Ingresar rendición historica
     */
    SE_IRH = 'SE-IRH',

    /**
     * Permiso: Enviar correciones a Dipres
     */
    SE_ECD = 'SE-ECD',

    // MODULO OBS / REVISION

    /**
     * Permiso: Ver justificación
     */
    RV_VJ = 'RV-VJ',

    /**
     * Permiso: Ingresar justificación
     */
    RV_IJ = 'RV-IJ',

    /**
     * Permiso: Ingresar respuesta justificación
     */
    RV_IRJ = 'RV-IRJ',
    /**
     * Permiso: Ver chat de revisión
     */
    RV_VCR = 'RV-VCR',

    /**
     * Permiso: Ingresar mensaje chat de revisión
     */
    RV_ICR = 'RV-ICR',

    /**
     * Permiso: Ver observaciones generales Dipres
     */
    RV_OGD = 'RV-VOG',

    /***
     * Permiso: Ingresar observaciones generales Dipres
     */
    RV_IOG = 'RV-IOG',

    // MODULO USUARIOS

    /**
     * Permiso: Acceso buscador usuarios
     */
    US_ABU = 'US-ABU',

    /**
     * Permiso: Gestionar Usuarios
     */
    US_GU = 'US-GU',

    /**
     * Permiso: Ver Usuarios
     */
    US_VU = 'US-VU',

    // OTROS

    /**
     * Permiso: Gestionar Carga
     */
    CM_GC = 'CM-GC',

    /**
     * Permiso: Gestionar Mantenedores
     */
    AD_GM = 'AD-GM',

    /**
     * Permiso: Reporte 1
     */
    RE_R1 = 'RE-R1',

    /**
     * Permiso: Reporte 2
     */
    RE_R2 = 'RE-R2',

    /**
     * Permiso: Reporte 3
     */
    RE_R3 = 'RE-R3',

    /**
     * Permiso: Reporte 4
     */
    RE_R4 = 'RE-R4',

    /**
     * Permiso: Reporte 5
     */
    RE_R5 = 'RE-R5',

    /**
     * Permiso: Reporte 6
     */
    RE_R6 = 'RE-R6',

    /**
     * Permiso: Reporte 7
     */
    RE_R7 = 'RE-R7',

    /**
     * Permiso: Ingreso de Observaciones Dipres Validador
     */

    OB_DIP = 'OB-DIP',

    /**
     * Permiso: Ingreso de Comentarios Institucion Iniciativa/Plan/UCR
     */

    OB_ICP = 'OB-ICP'
}