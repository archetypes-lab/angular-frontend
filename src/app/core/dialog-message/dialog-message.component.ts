import { Component, Injector, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { BlockuiService } from '../services/blockui.service';
import { DialogMessageService, DialogMessageData, Severity } from '../services/dialog-message.service';

@Component({
    selector: 'app-global-message',
    templateUrl: './dialog-message.component.html'
})
export class DialogMessageComponent implements OnDestroy {

    readonly BASECLASS: string = 'ui-global-message-dialog';

    title: string;
    message: string;
    classes: string;
    dialogVisible: boolean;
    closeButtonLabel: string;

    closeFunction?: Function;

    private openDialogSubscription: Subscription;

    private translateService: TranslateService;
    private dialogMessageService: DialogMessageService;
    private blockui: BlockuiService;

    constructor(injector: Injector) {
        this.translateService = injector.get(TranslateService);
        this.dialogMessageService = injector.get(DialogMessageService);
        this.blockui = injector.get(BlockuiService);
        this.dialogVisible = false;
        this.openDialogSubscription = this.dialogMessageService.openDialog$.subscribe(
            (parameters: DialogMessageData) => this.eventListener(parameters));
    }

    ngOnDestroy(): void {
        if (this.openDialogSubscription) {
            this.openDialogSubscription.unsubscribe();
        }
    }

    cerrarDialog(): void {
        this.dialogVisible = false;
        if (this.closeFunction) {
            this.closeFunction();
        }
        this.blockui.tick();
        this.dialogMessageService.notifyCloseDialog();
    }

    private async eventListener(data: DialogMessageData) {
        this.title = data.title;
        this.message = data.message;
        this.classes = this.loadClasses(data.severity);

        if (data.closeButtonLabelCode) {
            this.closeButtonLabel = this.translateService.instant(data.closeButtonLabelCode);
        }

        this.closeFunction = data.closeFunction ? data.closeFunction : () => { };

        this.dialogVisible = true;
        this.blockui.tick();
    }

    private loadClasses(severity: Severity) {
        let severityClass = '';
        switch (severity) {
            case Severity.Info:
                severityClass = 'info';
                this.closeButtonLabel = this.translateService.instant('labels.aceptar');
                break;
            case Severity.Success:
                severityClass = 'success';
                this.closeButtonLabel = this.translateService.instant('labels.aceptar');
                break;
            case Severity.Error:
                severityClass = 'error';
                this.closeButtonLabel = this.translateService.instant('labels.cerrar');
                break;
        }
        return this.BASECLASS + ' ' + severityClass;
    }
}
