import { Component, Injector, OnInit } from '@angular/core';
import { AuthService } from 'app/core/services/auth.service';
import { BreadcrumbService } from 'app/core/services/breadcrumb.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  private breadcrumbService: BreadcrumbService;
  private authService: AuthService;

  isLoggedIn: boolean;

  public constructor(private injector: Injector) {
    this.breadcrumbService = injector.get(BreadcrumbService);
    this.authService = injector.get(AuthService);
  }

  ngOnInit(): void {
    this.breadcrumbService.configDeshabilitado();
    this.isLoggedIn = this.authService.isLogged();
  }
}
