import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng';
import { AuthService } from '../../core/services/auth.service';
import { ConstantesUtils } from '../../core/utils/constantes.utils';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    private readonly authService: AuthService;
    private readonly router: Router;
    private readonly messageService: MessageService;

    username: string;
    password: string;
    msgs = [];

    ngOnInit() { }

    constructor(private injector: Injector) {
        this.authService = injector.get(AuthService);
        this.router = injector.get(Router);
        this.messageService = injector.get(MessageService);
    }

    async login() {
        try {
            await this.authService.login(this.username, this.password);
            this.router.navigate([ConstantesUtils.ROUTE_INICIO]);
        } catch (error) {
            this.messageService.add(
                {
                    severity: 'error', summary: 'Error', detail: error
                }
            );
        }
    }

}
