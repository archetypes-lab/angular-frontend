import { ApplicationRef, Component, OnDestroy, OnInit } from '@angular/core';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { InfoRestService } from 'app/core/api';
import { LocalStorage } from 'ngx-webstorage';
import { AuthService } from '../../core/services/auth.service';

@Component({
    selector: 'app-idle-detector',
    templateUrl: './idle-detector.component.html'
})
export class IdleDetectorComponent implements OnInit, OnDestroy {

    private readonly IDLE_TIMEOUT_TIME_SECONDS: number = 10;

    @LocalStorage()
    displayExpirada: boolean;

    displayWarning: boolean;


    countdown: number;

    timeout: boolean;

    constructor(private appRef: ApplicationRef, private idle: Idle, private authService: AuthService, private infoRestService: InfoRestService) { }

    async ngOnInit() {

        this.displayWarning = false;
        this.timeout = false;

        const infosistema = await this.infoRestService.getInfoUsingGET().toPromise();

        const idleTimeSeconds: number = infosistema.sesionTimeoutSegundos ?? 0;
        console.log(`idleTimeSeconds: ${idleTimeSeconds}`);

        this.idle.setIdle(idleTimeSeconds);
        this.idle.setTimeout(this.IDLE_TIMEOUT_TIME_SECONDS);
        this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

        this.idle.onIdleEnd.subscribe(async () => {
            console.log("onIdleEnd");
            this.displayWarning = false;
            this.appRef.tick();
        });

        this.idle.onTimeoutWarning.subscribe((countdown) => {
            console.log("timeout warning")
            this.displayWarning = true;
            this.countdown = countdown;
            this.appRef.tick();
        });

        this.idle.onTimeout.subscribe(async () => {
            console.log("timeout!");
            this.countdown = undefined;
            this.timeout = true;
            this.displayExpirada = true;
            this.displayWarning = false;
            this.authService.logout();
        });

        if (this.authService.isLogged()) {
            this.idle.watch();
        }

    }

    ngOnDestroy(): void {
        this.idle.clearInterrupts();
        this.idle.stop();
    }

    async expired() {
        this.displayExpirada = false;
    }

    extender(): void {
        this.idle.interrupt();
    }

}
