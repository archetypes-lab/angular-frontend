import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { IdentidadService } from 'app/core/services/identidad.service';
import { IdentidadDto } from '../../core/dto/identidad.dto';
import { MenuSuperiorComponent } from '../menu-superior/menu-superior.component';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  static readonly COLOR_BOTON_ACTIVO: string = "#0064ac";

  /* inyecciones */
  private readonly identidadService: IdentidadService;
  private readonly renderer: Renderer2;


  @ViewChild('menuMobileButton', { static: true }) menuMobileButton: ElementRef;
  @ViewChild('menuSuperior', { static: true }) menuSuperior: MenuSuperiorComponent;

  constructor(private injector: Injector) {
    this.identidadService = injector.get(IdentidadService);
    this.renderer = injector.get(Renderer2);
  }

  ngOnInit(): void {
    //vacio
  }

  public get identidad(): IdentidadDto {
    return this.identidadService.identidad;
  }

  public toggleMenuMovil() {
    this.menuSuperior.toggleMenuMovil();

    //actualizar el color del boton
    const backgroundColor = this.menuMobileButton.nativeElement.style.backgroundColor;
    if (backgroundColor === "" || backgroundColor === TopbarComponent.COLOR_BOTON_ACTIVO) {
      this.renderer.setStyle(this.menuMobileButton.nativeElement, "background-color", TopbarComponent.COLOR_BOTON_ACTIVO);
    }
    else {
      this.renderer.removeStyle(this.menuMobileButton.nativeElement, "background-color");
    }

  }

}

