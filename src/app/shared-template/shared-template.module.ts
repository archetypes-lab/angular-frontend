import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmDialogModule, DialogModule, MenubarModule, OverlayPanelModule, PanelMenuModule } from 'primeng';
import { BreadcrumbModule } from 'primeng/breadcrumb';
/* primeng */
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { BotonLoginComponent } from './boton-login/boton-login.component';
import { BotonLogoutComponent } from './boton-logout/boton-logout.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { FooterComponent } from './footer/footer.component';
import { IdleDetectorComponent } from './idle-detector/idle-detector.component';
import { MenuSuperiorComponent } from './menu-superior/menu-superior.component';
import { TemplateComponent } from './template/template.component';
import { TopbarComponent } from './topbar/topbar.component';


@NgModule({
  declarations: [
    TemplateComponent,
    FooterComponent,
    TopbarComponent,
    MenuSuperiorComponent,
    BreadcrumbComponent,
    IdleDetectorComponent,
    BotonLoginComponent,
    BotonLogoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    /*primeng*/
    BreadcrumbModule,
    DialogModule,
    ButtonModule,
    ScrollPanelModule,
    MenubarModule,
    PanelMenuModule,
    ScrollPanelModule,
    OverlayPanelModule,
    ConfirmDialogModule,
    TranslateModule
  ],
  exports: [TemplateComponent]
})
export class SharedTemplateModule { }
