import { Component, ElementRef, Injector, OnInit, Renderer2, ViewChild } from '@angular/core';
import { IdentidadDto } from 'app/core/dto/identidad.dto';
import { IdentidadService } from 'app/core/services/identidad.service';
import { MenuItem } from 'primeng/api';
import { ConstantesUtils } from '../../core/utils/constantes.utils';


@Component({
    selector: 'app-menu-superior',
    templateUrl: './menu-superior.component.html',
    styleUrls: ['./menu-superior.component.css']
})
export class MenuSuperiorComponent implements OnInit {

    items: MenuItem[];

    private identidadService: IdentidadService;

    private renderer: Renderer2;

    @ViewChild("menuSuperiorMovil", { static: true }) menuSuperiorMovil: ElementRef;

    public constructor(private injector: Injector) {
        this.identidadService = injector.get(IdentidadService);
        this.renderer = injector.get(Renderer2);
    }

    public ngOnInit() {
        this.items = [
            {
                label: 'Home',
                routerLink: '/',
                icon: 'fa fa-fw fa-home',
                routerLinkActiveOptions: { exact: true },
                title: 'Ir a la página inicial'
            },
            {
                label: 'Showcase',
                routerLink: ['/' + ConstantesUtils.ROUTE_SHOWCASE],
                icon: 'fa fa-fw fa-shopping-basket',
                title: 'Ir al showcase',
                visible: this.identidadService.tieneIdentidad()
            },
            {
                label: 'Administración',
                visible: this.identidadService.esAdministador(),
                icon: 'fa fa-fw fa-cogs',
                title: 'Menu administración',
                items: [
                    {
                        label: 'Gestionar Usuarios',
                        routerLink: ['/' + ConstantesUtils.ROUTE_GESTIONAR_USUARIOS],
                        title: 'Ir a gestión de Usuarios',
                        icon: 'fa fa-fw fa-users',
                        visible: this.identidadService.esAdministador()
                    },

                ]
            },
        ];
    }

    public get identidad(): IdentidadDto {
        return this.identidadService.identidad;
    }

    public toggleMenuMovil() {
        const rightStyle = this.menuSuperiorMovil.nativeElement.style.right;
        let rightNew: string;
        if (rightStyle === "-100%" || rightStyle === "") {
            rightNew = "0%";
        }
        else {
            rightNew = "-100%";
        }
        this.renderer.setStyle(this.menuSuperiorMovil.nativeElement, 'right', rightNew);
    }

}
