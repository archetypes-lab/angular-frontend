import { Component, Injector, OnDestroy } from '@angular/core';
import { BreadcrumbDto } from 'app/core/dto/breadcrumb.dto';
import { BreadcrumbService } from 'app/core/services/breadcrumb.service';
import * as _ from 'lodash';
import { MenuItem } from 'primeng/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnDestroy {

  private breadcrumbConfigSubscription: Subscription;
  private breadcrumbService: BreadcrumbService;

  deshabilitado: boolean;

  public constructor(private injector: Injector) {
    this.breadcrumbService = injector.get(BreadcrumbService);
    this.breadcrumbConfigSubscription = this.breadcrumbService.newConfig$
      .subscribe((breadcrumbs: BreadcrumbDto[]) => {
        this.config(breadcrumbs);
      });
  }

  ngOnDestroy(): void {
    this.breadcrumbConfigSubscription.unsubscribe();
  }

  items: MenuItem[];

  private config(breadcrumbs: BreadcrumbDto[]): void {
    this.deshabilitado = !breadcrumbs || breadcrumbs.length == 0;
    this.items = _.map(breadcrumbs, (dto: BreadcrumbDto) => {
      return <MenuItem>{
        label: dto.label,
        routerLink: dto.routerLink,
        icon: dto.icon
      };
    });
  }

}
