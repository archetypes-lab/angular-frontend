import { Component, Injector, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IdentidadDto } from "app/core/dto/identidad.dto";
import { IdentidadService } from "app/core/services/identidad.service";
import { ConstantesUtils } from "app/core/utils/constantes.utils";


@Component({
    selector: 'app-boton-login',
    templateUrl: './boton-login.component.html',
    styleUrls: ['./boton-login.component.css']
})
export class BotonLoginComponent implements OnInit {

    /* inyecciones */
    readonly identidadService: IdentidadService;
    readonly router: Router;

    constructor(injector: Injector) {
        this.router = injector.get(Router);
        this.identidadService = injector.get(IdentidadService);
    }

    ngOnInit(): void {
        // empty
    }

    public async iniciarSesionAction(): Promise<void> {
        this.router.navigate([ConstantesUtils.ROUTE_LOGIN]);
    }

    public get identidad(): IdentidadDto {
        return this.identidadService.identidad;
    }

}
