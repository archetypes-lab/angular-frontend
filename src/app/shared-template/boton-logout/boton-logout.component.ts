import { DOCUMENT } from "@angular/common";
import { Component, Injector } from "@angular/core";
import { AuthService } from "app/core/services/auth.service";
import { BlockuiService } from "app/core/services/blockui.service";
import { timer } from 'rxjs';
import { BotonLoginComponent } from "../boton-login/boton-login.component";


@Component({
    selector: 'app-boton-logout',
    templateUrl: './boton-logout.component.html',
    styleUrls: ['./boton-logout.component.css']
})
export class BotonLogoutComponent extends BotonLoginComponent {

    private readonly blockui: BlockuiService;
    private readonly authService: AuthService;
    private readonly document: any;


    constructor(injector: Injector) {
        super(injector);
        this.blockui = injector.get(BlockuiService);
        this.authService = injector.get(AuthService);
        this.document = injector.get(DOCUMENT);
    }

    ngOnInit(): void {
    }

    public async cerrarSesion(): Promise<void> {
        this.blockui.block();
        await this.authService.logout();
        timer(500).subscribe(() => {
            this.blockui.tick();
            this.document.location.reload();
        });
    }

}