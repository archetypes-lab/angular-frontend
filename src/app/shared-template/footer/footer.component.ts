import { Component, OnInit } from '@angular/core';
import { InfoRestService, VersionRestService } from 'app/core/api';
import { DateUtils } from 'app/core/utils/date.utils';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  version: string;

  fecha: Date;

  constructor(private infoRestService: InfoRestService, private versionRestService: VersionRestService) { }

  async ngOnInit() {
    const info = await this.infoRestService.getInfoUsingGET().toPromise();
    this.fecha = DateUtils.stringToDate(info.fechaPresente);
    const versionResponse = await this.versionRestService.getVersionUsingGET().toPromise();
    this.version = versionResponse['version'];
  }

}
