import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './core/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private translate: TranslateService, private authService: AuthService) {
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('es');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('es');
    }

    async ngOnInit(): Promise<void> {
        await this.authService.initSecurity();
    }
}
