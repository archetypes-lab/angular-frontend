import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerTextoDialogComponent } from './ver-texto-dialog.component';

describe('VerTextoDialogComponent', () => {
  let component: VerTextoDialogComponent;
  let fixture: ComponentFixture<VerTextoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerTextoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerTextoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
