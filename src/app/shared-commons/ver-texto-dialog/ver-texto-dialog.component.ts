import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ver-texto-dialog',
  templateUrl: './ver-texto-dialog.component.html',
  styleUrls: ['./ver-texto-dialog.component.css']
})
export class VerTextoDialogComponent implements OnInit {

  @Input() texto: string;
  @Input() title: string;
  @Input() sizeCut?: number;
  @Input() tooltip: string;

  dialogoVisible: boolean;
  constructor() { }

  ngOnInit() {
    this.dialogoVisible = false;
    this.sizeCut = this.sizeCut ? this.sizeCut : 14;
    this.title = this.title ? "Ver " + this.title : "Ver Texto";
  }

  get tooltipText(): string {
    return this.tooltip ? "Ver " + this.tooltip : "Ver texto";
  }
  
  abrirDialogo(): void {
    this.dialogoVisible = true;
  }

  cerrarDialogo(): void {
    this.dialogoVisible = false;
  }

}
