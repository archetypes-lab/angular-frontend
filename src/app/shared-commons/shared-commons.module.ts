import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule, DropdownModule, FieldsetModule, InputTextareaModule, InputTextModule, ToggleButtonModule } from 'primeng';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { RutTextPipe } from './pipes/ruttext.pipe';
import { SeparadorMilesPipe } from './pipes/separador-miles.pipe';
import { VerTextoDialogComponent } from './ver-texto-dialog/ver-texto-dialog.component';

@NgModule({
  declarations: [
    RutTextPipe,
    VerTextoDialogComponent,
    SeparadorMilesPipe
  ],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    InputTextareaModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ToggleButtonModule,
    CalendarModule,
    DropdownModule,
    VirtualScrollerModule,
    FieldsetModule,
    InputTextModule,
    ToggleButtonModule
  ],
  entryComponents: [
  ],
  exports: [
    RutTextPipe,
    VerTextoDialogComponent,
    SeparadorMilesPipe
  ]
})
export class SharedCommonsModule { }
