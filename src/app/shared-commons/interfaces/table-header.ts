export interface TableHeader {
    field: string;
    header: string;
    style: object;
    sortable: boolean;
}
