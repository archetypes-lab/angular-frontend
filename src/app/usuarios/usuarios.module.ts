import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FileUploadModule, MessageModule } from 'primeng';
/* primeng */
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { SharedCommonsModule } from '../shared-commons/shared-commons.module';
import { SharedTemplateModule } from '../shared-template/shared-template.module';
import { EditarComponent } from './editar/editar.component';
import { IndexComponent } from './index/index.component';
import { UsuariosRoutingModule } from './usuarios-routing.module';

@NgModule({
  declarations: [
    EditarComponent,
    IndexComponent,],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedCommonsModule,
    SharedTemplateModule,
    UsuariosRoutingModule,
    /*primeng*/
    MessageModule,
    ButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    TableModule,
    DialogModule,
    DropdownModule,
    InputTextModule,
    TooltipModule,
    FieldsetModule,
    InputTextareaModule,
    InputMaskModule,
    FileUploadModule
  ]
})
export class UsuariosModule { }
