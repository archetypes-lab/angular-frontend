import { Usuario } from "app/core/api";

export interface TablaUsuarioFila {
    username: string;
    run: number;
    runFormato: string;
    nombres: string;
    apellidos: string;
    perfil: string;
    fechaNacimiento: string;
    habilitado: string;
    usuario: Usuario;
}