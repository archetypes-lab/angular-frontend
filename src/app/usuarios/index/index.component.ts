import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PerfilRestService, Usuario, UsuarioFiltroDto, UsuarioRestService } from 'app/core/api';
import { PerfilUsuarioAux } from 'app/core/model-aux/perfil-usuario.aux';
import { AppConfigService } from 'app/core/services/app-config.service';
import { BlockuiService } from 'app/core/services/blockui.service';
import { BreadcrumbService } from 'app/core/services/breadcrumb.service';
import { SimpleMessageService } from 'app/core/services/simple-message.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ConfirmationService, LazyLoadEvent, SelectItem } from 'primeng/api';
import { RutConverter } from '../../core/converters/rut.converter';
import { FlashScopeService } from '../../core/services/flash-scope.service';
import { ConstantesUtils } from '../../core/utils/constantes.utils';
import { LocaleUtils } from '../../core/utils/locale.utils';
import { CustomsValidators } from '../../core/validators/customs.validators';
import { TablaUsuarioFila } from './tabla-usuario-fila';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

    private usuarioService: UsuarioRestService;
    private perfilService: PerfilRestService;
    private router: Router;
    private flashScope: FlashScopeService;
    private fb: FormBuilder;
    private confirmationService: ConfirmationService;
    private translate: TranslateService;
    private breadcrumbService: BreadcrumbService;
    private blockuiService: BlockuiService;
    private simpleMessageService: SimpleMessageService;
    private appConfigService: AppConfigService;

    formFiltros: FormGroup;
    formPass: FormGroup;
    passDialogVisible: boolean;
    formPassSubmitted: boolean;

    perfilesSelectItems: SelectItem[];
    habilitadosSelectItems: SelectItem[];

    busquedaRowCount: number;
    busquedaTimeout: NodeJS.Timer;
    tablaResultadosVisible: boolean;

    columnasTabla: any[];
    dataTabla: TablaUsuarioFila[];

    userSelected: Usuario;
    imgPerfilDialogVisible: boolean;
    urlImagen: string;
    tieneImagen: boolean;

    /* calendar */
    minDate: Date;
    maxDate: Date;
    yearRange: string;
    calLocale: any;

    constructor(private injector: Injector) {
        this.usuarioService = injector.get(UsuarioRestService);
        this.perfilService = injector.get(PerfilRestService);
        this.router = injector.get(Router);
        this.flashScope = injector.get(FlashScopeService);
        this.fb = injector.get(FormBuilder);
        this.confirmationService = injector.get(ConfirmationService);
        this.translate = injector.get(TranslateService);
        this.breadcrumbService = injector.get(BreadcrumbService);
        this.blockuiService = injector.get(BlockuiService);
        this.simpleMessageService = injector.get(SimpleMessageService);
        this.appConfigService = injector.get(AppConfigService);
    }

    /* publicos */

    public async ngOnInit() {
        this.blockuiService.block();
        this.breadcrumbService.configDesdeModUsuarios();
        this.columnasTabla = this.crearColumnasTablaUsuarios();
        this.userSelected = null;
        this.passDialogVisible = false;
        this.imgPerfilDialogVisible = false;
        this.formPassSubmitted = false;
        this.tablaResultadosVisible = false;
        this.initCalendar();
        this.habilitadosSelectItems = ConstantesUtils.createHabilitadosSelectItems();
        this.cargarPerfiles();
        this.crearFormFiltros();
        await this.buscar();
        this.blockuiService.tick();
    }

    public irNuevoUsuario(): void {
        this.router.navigate([ConstantesUtils.ROUTE_EDITAR_USUARIO]);
    }

    public irEditarUsuario(usuario: Usuario): void {
        this.flashScope.add(ConstantesUtils.FS_KEY_ID_USUARIO, usuario.idPersona);
        this.router.navigate([ConstantesUtils.ROUTE_EDITAR_USUARIO]);
    }

    public async loadBusquedaLazy(event: LazyLoadEvent) {
        this.blockuiService.block();
        if (!this.tablaResultadosVisible) {
            return;
        }
        const filtrosDto: UsuarioFiltroDto = this.formFiltrosToDto();
        const inicio = event.first;
        const fin = event.first + event.rows;

        const usuarios = await this.usuarioService.buscarUsingPOST(fin, inicio, filtrosDto)
            .toPromise();

        this.dataTabla = usuarios.map(this.mapUsuarioToFila);
        this.blockuiService.tick();
    }

    private mapUsuarioToFila(usuario: Usuario): TablaUsuarioFila {
        return <TablaUsuarioFila>{
            usuario: usuario,
            username: usuario.username,
            run: usuario.persona.run,
            runFormato: RutConverter.toString(usuario.persona.run),
            nombres: usuario.persona.nombres,
            apellidos: `${usuario.persona.apellidoPaterno} ${usuario.persona.apellidoMaterno}`,
            fechaNacimiento: moment.utc(usuario.persona.fechaNacimiento).format('DD/MM/YYYY'),
            perfil: usuario.perfil.nombre,
            habilitado: usuario.habilitado ? 'Sí' : 'No'
        };
    }

    public async confirmarEliminar(usuarioDto: Usuario) {
        this.confirmationService.confirm({
            message: this.translate.instant('dialogs.msg.eliminar-usuario'),
            accept: async () => {
                console.log('acepta eliminar!');
                await this.eliminar(usuarioDto);
                await this.buscar();
            }
        });
    }

    public async abrirDialogImgPerfil(usuario: Usuario) {
        this.blockuiService.block();
        this.userSelected = usuario;
        await this.refrescarUrlImage(usuario);
        this.imgPerfilDialogVisible = true;
        this.blockuiService.tick();
    }

    public abrirDialogCambiarPass(usuario: Usuario) {
        this.crearFormPass(usuario);
        this.passDialogVisible = true;
    }

    public cerrarDialogCambiarPass() {
        this.formPass = null;
        this.passDialogVisible = false;
    }

    public cerrarDialogImgPerfil() {
        this.userSelected = null;
        this.imgPerfilDialogVisible = false;
    }

    public async aceptarCambiarPass() {
        this.formPassSubmitted = true;
        if (!this.formPass.valid) {
            this.simpleMessageService
                .defaultErrorMsg(this.translate.instant('dialogs.msg.form-invalido'));
            return;
        }
        const usuario: Usuario = this.formPass.get('usuarioDto').value;
        const newPass: string = this.formPass.get('password').value;
        await this.usuarioService.cambiarPassUsingPOST(usuario.idPersona, newPass).toPromise();
        this.simpleMessageService
            .defaultSuccessMsg(this.translate.instant('info.msg.exito'));
        this.cerrarDialogCambiarPass();
    }

    public async subirImagenPerfil(event) {
        this.blockuiService.block();
        var file = event.files[0];
        await this.usuarioService.uploadImagePerfilUsingPOST(this.userSelected.idPersona, file).toPromise();
        await this.refrescarUrlImage(this.userSelected);
        this.blockuiService.tick();
    }

    public async borrarImagenPerfil() {
        this.blockuiService.block();
        await this.usuarioService.borrarImagenPerfilUsingGET(this.userSelected.idPersona).toPromise();
        await this.refrescarUrlImage(this.userSelected);
        this.blockuiService.tick();
    }


    /* privados */

    private formFiltrosToDto(): UsuarioFiltroDto {
        const run = this.formFiltros.get('run').value ? RutConverter.toNumber(this.formFiltros.get('run').value) : null;
        const filtrosDto: UsuarioFiltroDto = this.formFiltros.value;
        filtrosDto.run = run;
        return filtrosDto;
    }

    private async buscar() {
        if (!this.formFiltros.valid) {
            return;
        }
        clearTimeout(this.busquedaTimeout);
        this.busquedaTimeout = setTimeout(async () => {
            const filtrosDto: UsuarioFiltroDto = this.formFiltrosToDto();
            const rowCount = await this.usuarioService.buscarRowCountUsingPOST(filtrosDto).toPromise();
            this.busquedaRowCount = rowCount;
            this.refrescarTablaResultados();
        }, 600);
    }

    private async cargarPerfiles() {
        const perfiles = await this.perfilService.traerTodosUsingGET().toPromise();
        this.perfilesSelectItems = _.concat([ConstantesUtils.createTodosSelectItem()],
            _.map(perfiles, PerfilUsuarioAux.perfilToSelectItem));
    }

    private crearFormFiltros(): void {
        this.formFiltros = this.fb.group({
            username: [null,
                Validators.compose([Validators.minLength(4), Validators.maxLength(20), CustomsValidators.validateUsername])],
            perfil: [null],
            habilitado: [null],
            run: [null, CustomsValidators.validateRUN],
            nombres: [null, Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            apPaterno: [null, Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            apMaterno: [null, Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            email: [null, Validators.compose([CustomsValidators.validateEmail, Validators.maxLength(100)])],
            fechaNacimientoInferior: [null],
            fechaNacimientoSuperior: [null],
        });

        // listeners
        this.formFiltros.valueChanges
            .subscribe(val => this.buscar());
    }

    private crearFormPass(usuario: Usuario): void {
        this.formPass = this.fb.group({
            usuarioDto: [usuario],
            password: [null,
                Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
            confirmPassword: [null,
                Validators.required]
        }, { validator: CustomsValidators.matchingPasswords('password', 'confirmPassword') });
        // listeners
        this.formPass.get('password').valueChanges
            .subscribe(val => this.formPass.get('confirmPassword').updateValueAndValidity());
    }

    private initCalendar(): void {
        this.maxDate = new Date();
        this.minDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        this.minDate.setFullYear(this.minDate.getFullYear() - 90);
        this.yearRange = this.minDate.getFullYear() + ':' + this.maxDate.getFullYear();
        this.calLocale = LocaleUtils.getCalendarEs();
    }

    private refrescarTablaResultados(): void {
        this.tablaResultadosVisible = false;
        setTimeout(() => this.tablaResultadosVisible = true, 0);
    }

    private async eliminar(usuarioDto: Usuario) {
        await this.usuarioService.eliminarUsingPOST(usuarioDto.idPersona).toPromise();
        this.simpleMessageService
            .defaultSuccessMsg(this.translate.instant('info.msg.exito'));
    }

    private crearColumnasTablaUsuarios(): any[] {
        return [
            { field: 'username', header: 'Usuario', style: { 'width': '70px' }, sortable: false },
            { field: 'runFormato', header: 'RUN', style: { 'width': '115px' }, sortable: false },
            { field: 'nombres', header: 'Nombres', style: {}, sortable: false },
            { field: 'apellidos', header: 'Apellidos', style: {}, sortable: false },
            { field: 'perfil', header: 'Perfil', style: {}, sortable: false },
            { field: 'fechaNacimiento', header: 'Fecha Nacimiento', style: {}, sortable: false },
            { field: 'habilitado', header: 'Habilitado', style: { 'width': '89px' }, sortable: false },
            { field: 'acciones', header: 'Acciones', style: { 'width': '169px' }, sortable: false }
        ];
    }

    private async refrescarUrlImage(usuario: Usuario) {
        this.tieneImagen = await this.usuarioService.tieneImagenPerfilUsingGET(this.userSelected.idPersona).toPromise();
        if (this.tieneImagen) {
            this.urlImagen = `${this.appConfigService.apiUrl}/usuarios/descargar-imagen-perfil/${usuario.idPersona}`;
        } else {
            this.urlImagen = "";
        }
    }

}