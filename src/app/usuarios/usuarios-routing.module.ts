
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateComponent } from '../shared-template/template/template.component';
import { EditarComponent } from './editar/editar.component';
import { IndexComponent } from './index/index.component';



const routes: Routes = [
    {
        path: '', component: TemplateComponent,
        children: [
            {path: '', component: IndexComponent, outlet: 'content'}
        ]
    },
    {
        path: 'editar', component: TemplateComponent,
        children: [
            {path: '', component: EditarComponent, outlet: 'content'}
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsuariosRoutingModule {}
