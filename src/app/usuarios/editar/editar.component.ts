import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GuardarUsuarioDto, PerfilRestService, Persona, Usuario, UsuarioRestService } from 'app/core/api';
import { BreadcrumbDto } from 'app/core/dto/breadcrumb.dto';
import { PerfilUsuarioAux } from 'app/core/model-aux/perfil-usuario.aux';
import { BlockuiService } from 'app/core/services/blockui.service';
import { BreadcrumbService } from 'app/core/services/breadcrumb.service';
import { DialogMessageService } from 'app/core/services/dialog-message.service';
import { ConstantesUtils } from 'app/core/utils/constantes.utils';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { timer } from 'rxjs';
import { RutConverter } from '../../core/converters/rut.converter';
import { FlashScopeService } from '../../core/services/flash-scope.service';
import { LocaleUtils } from '../../core/utils/locale.utils';
import { CustomsValidators } from '../../core/validators/customs.validators';


@Component({
    selector: 'app-editar',
    templateUrl: './editar.component.html'
})
export class EditarComponent implements OnInit {

    private usuarioService: UsuarioRestService;
    private perfilService: PerfilRestService;
    private router: Router;
    private flashScope: FlashScopeService;
    private fb: FormBuilder;
    private confirmationService: ConfirmationService;
    private breadcrumbService: BreadcrumbService;
    private blockuiService: BlockuiService;
    private dialogMessageService: DialogMessageService;
    private translate: TranslateService;

    modoEditar: boolean;
    usuarioEntrada: Usuario;
    formUsuario: FormGroup;
    submitted: boolean;
    perfilesSelectItems: SelectItem[];

    /* calendar */
    minDate: Date;
    maxDate: Date;
    yearRange: string;
    calLocale: any;

    /* aux */
    usernameTimeOut: NodeJS.Timer;
    emailTimeOut: NodeJS.Timer;
    runTimeOut: NodeJS.Timer;

    constructor(private injector: Injector) {
        this.usuarioService = injector.get(UsuarioRestService);
        this.perfilService = injector.get(PerfilRestService);
        this.router = injector.get(Router);
        this.flashScope = injector.get(FlashScopeService);
        this.fb = injector.get(FormBuilder);
        this.confirmationService = injector.get(ConfirmationService);
        this.breadcrumbService = injector.get(BreadcrumbService);
        this.blockuiService = injector.get(BlockuiService);
        this.dialogMessageService = injector.get(DialogMessageService);
        this.translate = injector.get(TranslateService);
    }

    async ngOnInit() {
        console.log('iniciando modulo edicion-creacion usuario');
        this.blockuiService.block();
        this.breadcrumbService.configDesdeModUsuarios([<BreadcrumbDto>{ label: 'editar' }]);
        this.initCalendar();
        const idUsuarioParam: number = this.flashScope.get(ConstantesUtils.FS_KEY_ID_USUARIO);
        if (idUsuarioParam) {
            const data = await this.usuarioService.traerPorIdUsingGET(idUsuarioParam).toPromise();
            if (data) {
                this.iniciarModoEditar(data);
            } else {
                this.iniciarModoCrear();
            }
        } else {
            this.iniciarModoCrear();
        }
        const perfiles = await this.perfilService.traerTodosUsingGET().toPromise();
        this.perfilesSelectItems = _.concat([ConstantesUtils.createSeleccionarSelectItem()],
            _.map(perfiles, PerfilUsuarioAux.perfilToSelectItem));
        this.blockuiService.tick();
    }

    confirmAceptar(): void {
        console.log('entre confirmAceptar');
        this.submitted = true;
        if (!this.formUsuario.valid) {
            return;
        }
        this.confirmationService.confirm({
            message: 'Está seguro que desea guardar el usuario?',
            accept: () => {
                console.log('acepta guardar!');
                this.guardar();
            }
        });
    }


    volver(): void {
        this.router.navigate([ConstantesUtils.ROUTE_GESTIONAR_USUARIOS]);
    }

    /* privados */

    private iniciarModoCrear(): void {
        this.modoEditar = false;
        this.usuarioEntrada = <Usuario>{};
        this.usuarioEntrada.perfil = null;
        this.usuarioEntrada.persona = <Persona>{};
        this.crearFormulario();
    }

    private iniciarModoEditar(usuario: Usuario): void {
        this.usuarioEntrada = usuario;
        this.modoEditar = true;
        this.crearFormulario();
    }

    private async crearFormulario() {
        const usuario: Usuario = this.usuarioEntrada;

        this.formUsuario = this.fb.group({
            id: [usuario.idPersona],
            habilitado: [usuario.habilitado],
            username: [usuario.username,
            Validators.compose([Validators.required, Validators.minLength(4),
            Validators.maxLength(20), CustomsValidators.validateUsername]), await this.usernameUnicoValidator()],
            perfil: [usuario.perfil,
            Validators.required],
            run: [usuario.persona.run ? RutConverter.toString(usuario.persona.run) : null,
            Validators.compose([Validators.required, CustomsValidators.validateRUN]), await this.runUnicoValidator()],
            nombres: [usuario.persona.nombres,
            Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
            apPaterno: [usuario.persona.apellidoPaterno,
            Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
            apMaterno: [usuario.persona.apellidoMaterno,
            Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
            email: [usuario.persona.email,
            Validators.compose([Validators.required, CustomsValidators.validateEmail, Validators.maxLength(100)]),
            await this.emailUnicoValidator()],
            fechaNacimiento: [usuario.persona.fechaNacimiento ? moment(usuario.persona.fechaNacimiento).toDate() : null,
            Validators.required]
        });
        // si es modo editar
        if (!this.modoEditar) {
            this.formUsuario.addControl('password', this.fb.control(null, Validators.compose([Validators.required, Validators.minLength(8),
            Validators.maxLength(20)])));
            this.formUsuario.addControl('confirmPassword', this.fb.control(null, Validators.required));
            this.formUsuario.setValidators(<ValidatorFn>CustomsValidators.matchingPasswords('password', 'confirmPassword'));
            // listeners
            this.formUsuario.get('password').valueChanges
                .subscribe(val => this.formUsuario.get('confirmPassword').updateValueAndValidity());
        }
    }

    private initCalendar(): void {
        this.maxDate = new Date();
        this.minDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        this.minDate.setFullYear(this.minDate.getFullYear() - 90);
        this.yearRange = this.minDate.getFullYear() + ':' + this.maxDate.getFullYear();
        this.calLocale = LocaleUtils.getCalendarEs();
    }

    private async guardar() {
        this.blockuiService.block();
        console.log('entre guardar');
        const usuario: Usuario = this.formToDto();
        const guardarDto = this.usuarioToUsuarioGuardarDto(usuario);
        await this.usuarioService.guardarUsingPOST(guardarDto).toPromise();
        console.log('éxito guardar!');
        this.blockuiService.tick();
        timer(100).subscribe(() => {
            this.dialogMessageService.showSuccess(
                this.translate.instant('dialogs.info.title'),
                'El Registro de usuario se ha realizado con éxito',
                () => { this.volver() });
        });
    }

    private formToDto(): Usuario {
        const usuario = <Usuario>{};
        usuario.persona = <Persona>{};
        usuario.idPersona = this.formUsuario.get('id').value;
        usuario.username = this.formUsuario.get('username').value;
        usuario.perfil = this.formUsuario.get('perfil').value;
        usuario.habilitado = this.formUsuario.get('habilitado').value;
        usuario.password = this.modoEditar ? this.usuarioEntrada.password : this.formUsuario.get('password').value;
        usuario.persona.idPersona = usuario.idPersona;
        usuario.persona.fechaNacimiento = this.formUsuario.get('fechaNacimiento').value;
        usuario.persona.email = this.formUsuario.get('email').value;
        usuario.persona.apellidoPaterno = this.formUsuario.get('apPaterno').value;
        usuario.persona.apellidoMaterno = this.formUsuario.get('apMaterno').value;
        usuario.persona.nombres = this.formUsuario.get('nombres').value;
        usuario.persona.run = RutConverter.toNumber(this.formUsuario.get('run').value);
        return usuario;
    }

    private usuarioToUsuarioGuardarDto(usuario: Usuario): GuardarUsuarioDto {
        return <GuardarUsuarioDto>{
            idPersona: usuario.idPersona,
            perfil: usuario.perfil,
            persona: usuario.persona,
            username: usuario.username,
            password: usuario.password
        };
    }

    /*
    Validadores Asincronos
    */

    private async usernameUnicoValidator(): Promise<AsyncValidatorFn> {
        return (control: AbstractControl): Promise<any> => {
            clearTimeout(this.usernameTimeOut);
            return new Promise<any>(async resolve => {
                this.usernameTimeOut = setTimeout(async () => {
                    try {
                        const user = await this.usuarioService.traerPorUsernameUsingGET(control.value).toPromise();
                        if (user && user.idPersona !== this.usuarioEntrada.idPersona) {
                            resolve({ usernameUnico: true });
                        } else {
                            resolve(null);
                        }
                    } catch (error) {
                        resolve({ usernameUnico: true });
                    }
                }, 1000);
            });
        };
    }

    private async emailUnicoValidator(): Promise<AsyncValidatorFn> {
        return (control: AbstractControl): Promise<any> => {
            clearTimeout(this.emailTimeOut);
            return new Promise<any>(resolve => {
                this.emailTimeOut = setTimeout(async () => {
                    try {
                        const user = await this.usuarioService.traerPorEmailUsingPOST(control.value).toPromise();
                        if (user && user.idPersona !== this.usuarioEntrada.idPersona) {
                            resolve({ emailUnico: true });
                        } else {
                            resolve(null);
                        }
                    } catch (error) {
                        resolve({ emailUnico: true });
                    }
                }, 1000);
            });
        };
    }

    private async runUnicoValidator(): Promise<AsyncValidatorFn> {
        return (control: AbstractControl): Promise<any> => {
            clearTimeout(this.runTimeOut);
            return new Promise<any>(resolve => {
                this.runTimeOut = setTimeout(async () => {
                    try {
                        const user = await this.usuarioService.traerPorRunUsingGET(RutConverter.toNumber(control.value)).toPromise();
                        if (user && user.idPersona !== this.usuarioEntrada.idPersona) {
                            resolve({ runUnico: true });
                        } else {
                            resolve(null);
                        }
                    } catch (error) {
                        resolve({ runUnico: true });
                    }
                }, 1000);
            });
        };
    }

}
