import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppConfigService } from 'app/core/services/app-config.service';
import { AppModule } from './app/app.module';

const appConfig = new AppConfigService();

appConfig.load().then(response => {

    const production = response.data.production;

    if (production) {
        enableProdMode();
        window.console.log = ()=>{};
        window.console.error = ()=>{};
    }

    platformBrowserDynamic().bootstrapModule(AppModule)
        .catch(err => console.log(err));
})

