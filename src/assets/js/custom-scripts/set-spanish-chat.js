function setSpanishChat() {
    var noMessagesParagraph = document.getElementsByClassName("no-messages");
    var inputs = document.getElementsByTagName("input");
    if(noMessagesParagraph && noMessagesParagraph.length > 0 && noMessagesParagraph.item(0).innerHTML === "No messages yet.") {
        noMessagesParagraph.item(0).innerHTML = "No se encontraron mensajes.";
    }
    
    Array.prototype.forEach.call(inputs, input => {
        if(input.placeholder === "Type a message") {
            input.placeholder = "Ingrese un mensaje";        
        }
    });
}