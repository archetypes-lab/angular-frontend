rm -f $RUTA_CONFIG_ENV_FILE
echo "{" >> $RUTA_CONFIG_ENV_FILE
echo "  \"production\": $PRODUCTION, " >> $RUTA_CONFIG_ENV_FILE
echo "  \"envName\": \"$ENV_NAME\", " >> $RUTA_CONFIG_ENV_FILE
echo "  \"apiUrl\": \"$API_URL\" " >> $RUTA_CONFIG_ENV_FILE
echo "}" >> $RUTA_CONFIG_ENV_FILE
nginx -g "daemon off;"
