FROM nginxinc/nginx-unprivileged

ENV RUTA_CONFIG_ENV_FILE=/usr/share/nginx/html/assets/config/env.json
ENV PRODUCTION=false
ENV ENV_NAME=docker
ENV API_URL=http://host.docker.internal:8080

COPY start-container.sh /tmp/start-container.sh
COPY nginx_default.conf /etc/nginx/conf.d/default.conf 
COPY dist/ /usr/share/nginx/html

USER 0
RUN chgrp +0 /usr/share/nginx/html \
    && chmod -R g=u /usr/share/nginx/html \
    && chmod +x /tmp/start-container.sh

USER 101

EXPOSE 8080

CMD /tmp/start-container.sh